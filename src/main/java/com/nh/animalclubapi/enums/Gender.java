package com.nh.animalclubapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Gender {
    MAN("남")
    , WOMAN("여");
    private final String name;
}
