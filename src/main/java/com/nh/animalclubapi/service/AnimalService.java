package com.nh.animalclubapi.service;

import com.nh.animalclubapi.entity.Animal;
import com.nh.animalclubapi.entity.Member;
import com.nh.animalclubapi.model.animal.AnimalCreateRequest;
import com.nh.animalclubapi.repository.AnimalRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AnimalService {
    private final AnimalRepository animalRepository;

    public void setAnimal(Member member, AnimalCreateRequest request){
        Animal addData = new Animal();
        addData.setMember(member);
        addData.setAnimal(request.getAnimal());

        animalRepository.save(addData);
    }

}
