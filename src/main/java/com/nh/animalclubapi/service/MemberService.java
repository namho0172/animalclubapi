package com.nh.animalclubapi.service;

import com.nh.animalclubapi.entity.Member;
import com.nh.animalclubapi.model.member.MemberCreateRequest;
import com.nh.animalclubapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long id){return memberRepository.findById(id).orElseThrow();}

    public void setMember(MemberCreateRequest request){
        Member addData = new Member();
        addData.setName(addData.getName());
        addData.setPhoneNumber(addData.getPhoneNumber());
        addData.setGender(addData.getGender());

        memberRepository.save(addData);
    }
}
