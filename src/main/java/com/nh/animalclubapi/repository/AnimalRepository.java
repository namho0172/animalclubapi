package com.nh.animalclubapi.repository;

import com.nh.animalclubapi.entity.Animal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnimalRepository extends JpaRepository<Animal, Long> {
}
