package com.nh.animalclubapi.repository;

import com.nh.animalclubapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
