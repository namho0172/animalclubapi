package com.nh.animalclubapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnimalClubApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnimalClubApiApplication.class, args);
	}

}
