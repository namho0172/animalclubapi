package com.nh.animalclubapi.model.animal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AnimalCreateRequest {
    private String animal;
}
