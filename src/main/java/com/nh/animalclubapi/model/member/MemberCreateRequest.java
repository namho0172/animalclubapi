package com.nh.animalclubapi.model.member;

import com.nh.animalclubapi.enums.Gender;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberCreateRequest {
    private String name;
    private String phoneNumber;
    private Gender gender;
}
